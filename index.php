﻿<?php
session_start();
if(isset($_SESSION['login'])){
	$_SESSION['login'] = $_SESSION['login'];
}
else{
	$_SESSION['login'] = "simple_user";
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Доска объявлений</title>
	<link rel="stylesheet" type="text/css" href="styles/style.css">
	<script type="text/javascript" src='scripts/angular.min.js'></script>
	<script type="text/javascript" src='scripts/jquery-2.1.4.min.js'></script>
	<script type="text/javascript" src='scripts/javascripte.js'></script>

</head>
<?php
require_once 'blocks/top.php';
?>
<body ng-app = "MyApp" ng-controller = "MyCtrl">
	<div id='categories'>
		<div class='fellas' id='categor_all' style="margin-left:-200px;width:715px;">
			<a href="search.php?name=Недвижимость"><div class='idies'><img src="images/home.png"  width="130px;" height="125px"><br>Недвижимость</div></a>
			<a href='search.php?name=Авто%20и%20мото'><div class='idies'><img src="images/car.png"  width="130px;" height="125px"><br>Авто и мото</div></a>
			<a href='search.php?name=Работа%20и%20образование'><div class='idies'><img src="images/job.png"  width="130px;" height="125px"><br>Работа и образование</div></a>
			<a href='search.php?name=Строительство%20и%20ремонт'><div class='idies'><img src="images/build.png"  width="130px;" height="125px"><br>Строительство и ремонт</div></a>
			<div id='other_link'>
			<ul>
				<a href="search.php?name=Компьютерная техника"><li>Компьютерная техника</li></a>
				<a href="search.php?name=Бытовая техника и электроника"><li>Бытовая техника и электроника</li></a>
				<a href="search.php?name=Фото, оптика"><li>Фото, оптика</li></a>
				<a href="search.php?name=Одежда, обувь, аксессуары"><li>Одежда, обувь, аксессуары</li></a>
				<a href="search.php?name=Товары для детей"><li>Товары для детей</li></a><br>
			</ul>
			<ul style="margin-left:0px;">
				<a href="search.php?name=Спорт, туризм и отдых"><li>Спорт, туризм и отдых</li></a>
				<a href="search.php?name=Мебель, интерьер, обиход"><li>Мебель, интерьер, обиход</li></a>
				<a href="search.php?name=Телефоны и связь"><li>Телефоны и связь</li></a>
				<a href="search.php?name=Животное и растения"><li>Животное и растения</li></a>
				<a href="search.php?name=Бизнес и партнёрство"><li>Бизнес и партнёрство</li></a>
			</ul>
			<ul style="margin-left:0px;">
				<a href="search.php?name=Музыка, искусство"><li>Музыка, искусство</li></a>
				<a href="search.php?name=Праздники и подарки"><li>Праздники и подарки</li></a>
				<a href="search.php?name=Книги, учебники и журналы"><li>Книги, учебники и журналы</li></a>
				<a href="search.php?name=Услуги и деятельность"><li>Услуги и деятельность</li></a>
			</ul>
			</div>
		</div>
		<div class='fellas' style="margin-left:20px;text-align:center;">
			<img src="images/adv.jpg" width="260px" height="400px"/>
		</div>
	</div>
	<div id='show_sost'>
		<div class='adv_all'><img src='images/ad1.jpg' width="220px" height="200px"><br><span class='text_out'>Бесплатное объявление</span><br><span class='text_in'>Приложения для iOS и Android дают возможность находить и размещать объявления, а также делать фотографии своих товаров и размещать их прямо со смартфона. Попробуйте, это удобно!</span></div>
		<div class='adv_all'><img src='images/ad2.jpg' width="220px" height="200px"><br><span class='text_out'>Эконом денег</span><br><span class='text_in'>Партнёрская программа помогает вашему бизнесу каждый день. Автоматическая загрузка ваших предложений и более заметное оформление увеличивают продажи</span></div>
		<div class='adv_all'><img src='images/ad3.jpg' width="220px" height="200px"><br><span class='text_out'>Выгодные предложении</span><br><span class='text_in'>Сервис прямого взаимодействия с банками позволяет сравнить условия и программы, а также отправить заявку на кредит прямо с сайта. Предложение о покупке в кредит находится внутри объявлений</span></div>
		<div class='adv_all'><img src='images/ad4.jpg' width="220px" height="200px"><br><span class='text_out'>Яркие идеи</span><br><span class='text_in'>Мы помогаем детям вернуть здоровье и надежду на счастливую интересную жизнь. Знаменитые люди представляют вещи из личных коллекций в специальном разделе</span></div>
	</div>
	<div id='bottom_in'>
		<ul>
			<li>Телефонные контакты:</li>
		</ul>
		<ul style='margin-left:-25px;'>
			<li>+ 7 (702) 233 33 30</li><br>
			<li>+ 7 (702) 233 33 31</li><br>
			<li>+ 7 (702) 233 33 32</li><br>
			<li>+ 7 (702) 233 33 33</li><br>
		</ul>
		<ul style='float:right;margin-right:100px;margin-top:30px;'>
			<li style="margin-right:10px;">Наши контакты в социальных сетях </li>
			<li><a href="https://ok.ru/" target="_blank"><img src="images/adv1.png" width="20px" height="20px"></a></li>
			<li><a href="https://vk.com/" target="_blank"><img src="images/adv2.png" width="20px" height="20px"></a></li>
			<li><a href="https://fb.com/" target="_blank"><img src="images/adv3.png" width="20px" height="20px"></a></li>
			<li><a href="https://instagram.com/" target="_blank"><img src="images/adv4.jpg" width="20px" height="20px"></a></li><br>
			<li style='cursor:pointer;'><ul style="margin-top:0px;"><li style='padding-left:40px;'><img src="https://d30y9cdsu7xlg0.cloudfront.net/png/17698-200.png" width="20px" height="20px"></li><li style='margin-left:10px;'>Оставить отзыв о сайте</li></ul></li>
		</ul>
		<div id='copyrights'>
		<ul>
			<li>&copyFinal. Сайт создан 2016-ом году </li><li><img src='http://www.alkorbio.ru/docs/image1/img666.jpg' width="30px" height="20px"></li>
		</ul>
		</div>
	</div>
</div>
</body>
</html>